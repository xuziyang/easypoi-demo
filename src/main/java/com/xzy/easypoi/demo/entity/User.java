package com.xzy.easypoi.demo.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Excel(name = "id")
    private Integer id;

    @Excel(name = "姓名")
    private String name;

}
